#ifndef CRC32_H
#define CRC32_H

uint32_t crc32(uint32_t, const void*, size_t);

#endif
